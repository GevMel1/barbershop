// var doc = document.querySelector(".login-link");
// var log = document.querySelector(".modal-login");
// var test = log.querySelector(".modal-close");
// var test2 = log.querySelector(".input");

// doc.addEventListener("click", function(test3) {
//   test3.preventDefault();
//   console.log("ыыыыыыыы!");
//   log.classList.add("modal-show");
//   test2.focus()
// });

// test.addEventListener("click", function(test3) {
//   test3.preventDefault();
//   console.log("ы!");
//   log.classList.remove("modal-show");
// });

// function randomNumber() {
//     console.log(Math.random())
// }

// setInterval(randomNumber, 200)

var mainNavElement = document.querySelector('.main-nav');
var navWrapperElement = document.querySelector('.main-nav__wrapper');
var toggleNavElement = document.querySelector('.main-nav__toggle');
var mainNavIcon = document.querySelector('.main-nav__icon');

toggleNavElement.addEventListener('click', function() {
    navWrapperElement.classList.toggle('main-nav__wrapper--hide');
    mainNavElement.classList.toggle('main-nav--toggle');
    changeNavIcon()
})

function changeNavIcon () {
    if (mainNavIcon.hasAttribute('width')) {
        mainNavIcon.setAttribute('src', 'img/nav-open-icon.svg')
        mainNavIcon.removeAttribute('width')
    } else {
        mainNavIcon.setAttribute('src', 'img/nav-close-icon.svg')
        mainNavIcon.setAttribute('width', '25px')
    }
}